package com.lid.viewpager.home.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.lid.viewpager.R;

import java.util.List;

public class HomeViewPagerAdapter extends PagerAdapter {

    private Context mContext;
    private List<String> mData;

    public HomeViewPagerAdapter(Context mContext, List<String> mData) // 构造器
    {
        this.mContext = mContext;
        this.mData = mData;
    }

    @Override
    public int getCount() // 根据 mData 的数量，创建 ViewPager 中的页面数量
    {
        return mData.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) // 好像是判断是否重复，如果已经加载了那就不去重复加载
    {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) // 实例化 pager.xml
    {
        View view = View.inflate(mContext, R.layout.pager, null); // 实例化 pager.xml

        TextView textView = view.findViewById(R.id.text_view); // 取得 pager.xml 中的 TextView
        textView.setText(mData.get(position)); // 根据 position 设置 TextView 中的文字

        container.addView(view); // 将实例化的 pager.xml 加入到容器中去, container 就是容器.
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) // 好像是销毁Item用的
    {
        container.removeView((View) object);
    }

    @Override
    public CharSequence getPageTitle(int position) // PagerTitleStrip 的标题就是从这里取到的
    {
        return mData.get(position);
    }
}
