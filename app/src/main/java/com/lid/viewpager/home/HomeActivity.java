package com.lid.viewpager.home;

import android.content.Context;
import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.lid.viewpager.R;
import com.lid.viewpager.home.adapter.HomeViewPagerAdapter;
import com.lid.viewpager.main.adapter.MainViewPagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class HomeActivity extends AppCompatActivity {

    public static void actionStart(Context context) {
        Intent intent = new Intent(context, HomeActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        setVp();
    }

    private void setVp() // 配置 ViewPager
    {

        List<String> list = new ArrayList<>();

        for (int i = 0; i < 3; i++) // 填充数据源
        {
            list.add("第" + i + "个View");
        }

        ViewPager vp = findViewById(R.id.vp); // 注意这里 R.id.vp

        vp.setAdapter(new HomeViewPagerAdapter(this, list)); // 设置适配器 ( Set Adapter )
    }
}
