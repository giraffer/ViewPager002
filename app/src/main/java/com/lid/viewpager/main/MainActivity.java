package com.lid.viewpager.main;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.lid.viewpager.R;
import com.lid.viewpager.home.HomeActivity;
import com.lid.viewpager.main.adapter.MainViewPagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setVp();

        findViewById(R.id.start_home).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.actionStart(MainActivity.this);
            }
        });
    }

    private void setVp() // 配置 ViewPager
    {

        List<String> list = new ArrayList<>();

        for (int i = 0; i < 3; i++) // 填充数据源
        {
            list.add("第" + i + "个View");
        }

        ViewPager vp = findViewById(R.id.view_pager);

        vp.setAdapter(new MainViewPagerAdapter(this, list)); // 设置适配器 ( Set Adapter )
    }
}
